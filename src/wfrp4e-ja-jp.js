// https://obfuscator.io
/**
 * Utility class with all predefined converters
 */
class ConvertersJa {
    static tableResults(results, translations, x, y, z) {
        return results.map(data => {
            if (translations) {
                const translation = translations[`${data.range[0]}-${data.range[1]}`];
                if (translation) {
                    return mergeObject(data, mergeObject({ 'text': translation }, { translated: true }));
                }
            }
            if (data.documentCollection) {
                const text = game.babele.translateField('name', data.documentCollection, { 'name': data.text });
                if (text) {
                    return mergeObject(data, mergeObject({ 'text': text }, { translated: true }));
                } else {
                    return data;
                }
            }
            return data;
        })
    }
    static translateActorEffects(effects, translation, actor, TranslatedCompendium, translations) {
        for (let i = 0; i < effects.length; i++) {
            game.wfrp4ejajp.translateEffect(effects[i], translation, actor, TranslatedCompendium, translations);
        }
        return effects;
    }
    static translateEffects(effects, translation, item, TranslatedCompendium, translations) {
        for (let i = 0; i < effects.length; i++) {
            game.wfrp4ejajp.translateEffect(effects[i], item);
        }
        return effects;
    }
    static unitLocalizeJa(value) {
        let text = value.toLowerCase();
        if (text == "days") return game.i18n.localize("Days");
        if (text == "hours") return game.i18n.localize("Hours");
        if (text == "minutes") return game.i18n.localize("Minutes");
        if (value) Wfrp4eJaJp.warn(`ConvertersJa.unitLocalizeJa：入力値が対象外です「${value}」`);
        return value
    }

    static getItemDict(dict_item, type) {
        let entries = dict_item[type];
        if (!entries) {
            dict_item[type] = { 'mapping': { 'description': "system.description.value", 'gmdescription': "system.gmdescription.value" }, 'entries': {} };
            entries = dict_item[type];
        }
        return entries
    }
    static getItemEntry(entries, name) {
        let entry = entries[name];
        if (entry) { return entry; }
        let item_name = name.trim();
        let item_spec = "";
        let startParen = name.indexOf("(");
        let endParen = name.indexOf(")", startParen);
        let isSpec = startParen != -1 && endParen != -1;

        if (isSpec) {
            item_name = name.substring(0, startParen).trim();
            item_spec = name.substring(startParen + 1, endParen).trim();
        }
        entry = entries[item_name];
        if (!entry) {
            entries[item_name] = { "name": item_name }
            entry = entries[item_name];
        }
        if (isSpec) {
            if (!("spec" in entry)) { entry.spec = {}; }
            ConvertersJa.translateHashes(entry.spec, item_spec);
        }
        return entry;
    }
    static getItemName(entry, name) {
        let tw_name = entry.name;
        if (entry.spec) {
            let startParen = name.indexOf("(");
            if (startParen != -1) {
                let endParen = name.indexOf(")", startParen);
                if (endParen != -1) {
                    let item_spec = name.substring(startParen + 1, endParen).trim();
                    if (!entry.spec[item_spec]) {
                        tw_name = tw_name + " (" + item_spec + ")";
                    } else {
                        tw_name = tw_name + " (" + entry.spec[item_spec] + ")";
                    }
                }
            }
        } else {
            // getItemEntryが先に呼ばれて、専門分野は登録済みの前提で処理を省略
        }
        return tw_name;
    }
    static translateItem(mapping, entry, object, isSystem, item) {
        let itemName = null;
        // マッピングの処理
        for (let key in mapping) {
            //========== Description 処理抑制（確認用） ==========
            if (game.settings.get("wfrp4e-ja-jp", "supressDescription") && key === "description") continue;
            //========== Description 処理抑制（確認用） ==========
            if (typeof mapping[key] === "string") {
                let path = mapping[key];
                if (!isSystem || path.substring(0, 7) === "system.") {
                    if (isSystem) { path = path.substring(7); }
                    let text = path.split('.').reduce((acc, val) => { return acc && acc[val] }, object);
                    if (text && !/^([ -+*/#0-9]+|[0-9]+d[0-9]+[ -+*/#0-9]+|[a-z]{1,3}|\s*)$/.test(text) && text != "<p></p>") {
                        if (typeof entry[key] === "string" && path === "name") {
                            // 泥縄的対処、名前は他のマップ処理後に反映するため一次格納
                            itemName = entry[key];
                        } else if (typeof entry[key] === "string") {
                            path.split('.').reduce((acc, val, idx, arr) => { if (idx == arr.length - 1) acc[val] = entry[key]; return acc[val]; }, object);
                        } else {
                            if (!(key in entry)) {
                                entry[key] = {};
                            }
                            path.split('.').reduce((acc, val, idx, arr) => {
                                if (idx == arr.length - 1) acc[val] = ConvertersJa.translateHashes(entry[key], text);
                                return acc[val];
                            }, object);
                        }
                    }
                }
            } else if (typeof mapping[key] === "object") {
                let path = mapping[key].path;
                if (!isSystem || path.substring(0, 7) === "system.") {
                    if (isSystem) { path = path.substring(7); }
                    let converter = Babele.get().converters[mapping[key].converter];
                    let text = path.split('.').reduce((acc, val) => { return acc && acc[val] }, object);
                    let tw_text = converter(text, null, item);
                    // 泥縄的対処、名前は他のマップ処理後に反映するため一次格納
                    if (path === "name") {
                        itemName = tw_text;
                    } else {
                        path.split('.').reduce((acc, val, idx, arr) => { if (idx == arr.length - 1) acc[val] = tw_text; return acc[val]; }, object);
                    }
                }
            }
        }
        // 泥縄的対処、対比した名前が存在した場合、反映
        if (itemName) {
            object["name"] = itemName;
        }
        return object;
    }
    static dictmap(data, dict, addDict, asHash = true, path = "") {
        if (!(dict instanceof Object) || dict instanceof Array) {
            Wfrp4eJaJp.warn(`ConvertersJa.dictmap: 辞書データがハッシュテーブルではありません。（${path}）`);
            return false;
        }
        if (data instanceof Array) {
            data.forEach(x => {
                if (x instanceof Array) {
                    ConvertersJa.dictmap(x, dict, addDict, asHash, path);
                } else if (x instanceof Object) {
                    // 配列の要素がオブジェクトの場合は、オブジェクトに次の要素が有る場合、辞書を参照、登録する。
                    // 「_id」、「name」、「label」
                    let id = x._id || x.name;
                    if (!id) {
                        id = x.label;
                    }
                    if (id) {
                        let translation = dict[x._id] || dict[x.name];
                        if (!translation && !x.name) {
                            translation = dict[x.label];    // nameが有る場合にlabelは過去互換で警告がでるので、頭悪い方法だけど……
                        }
                        if (addDict && !translation) {
                            dict[id] = {};
                            ConvertersJa.dictmap(x, dict[id], addDict, asHash, path + "." + id);
                        } else if (translation) {
                            ConvertersJa.dictmap(x, translation, addDict, asHash, path + "." + id);
                        }
                    } else {
                        ConvertersJa.dictmap(x, dict, addDict, asHash, path + "." + id);
                        if (path.split(".").pop() !== "changes") { // changesには無いので、警告抑制
                            Wfrp4eJaJp.warn(`ConvertersJa.dictmap: 配列「${path}」にid候補要素（「_id」、「name」、「label」）がありません。`);
                        }
                    }
                } else {
                    if (x in dict) {
                        x = dict[x];
                    } else if (addDict) {
                        dict[x] = x;
                    }
                }
            });
        } else if (data instanceof Object) {
            Object.keys(data).forEach(key => {
                if (!(key in dict)) {
                    if (!addDict) return;
                    if (!asHash && !(data[key] instanceof Object)) {
                        dict[key] = data[key];
                        return;
                    } else {
                        dict[key] = {};
                    }
                }
                if (data[key] instanceof Object) {
                    ConvertersJa.dictmap(data[key], dict[key], addDict, asHash, path + "." + key);
                } else if (asHash && dict[key] instanceof Object) {
                    if (data[key] in dict[key]) {
                        data[key] = dict[key][data[key]];
                    } else {
                        dict[key][data[key]] = data[key];
                    }
                } else {
                    if (key in dict) {
                        data[key] = dict[key];
                    } else {
                        dict[key] = data[key];
                    }
                }
            });
        } else {
            Wfrp4eJaJp.warn("ConvertersJa.dictmap: 処理対象がハッシュテーブルや配列ではありません。");
            return false;
        }
    }
    static translateHashes(hashes, data) {
        let value = data.trim()
        let flgExport = !game.settings.get("wfrp4e-ja-jp", "supressExportDict") ||
            !/^([ -+*/#0-9]+|[0-9]+d[0-9]+[ -+*/#0-9]+|\s*)$/.test(value) &&
            value != "<p></p>" &&
            !["ws", "bs", "s", "t", "i", "ag", "dex", "int", "wp", "fel"].includes(value);
        let tw_value = hashes[value];
        if (!tw_value && !value.startsWith("<p>")) {
            tw_value = "<p>" + value + "</p>";
            tw_value = hashes[tw_value];
        }
        if (!tw_value) {
            // 未登録の場合、辞書に登録
            if (flgExport) hashes[value] = value;
            return value;
        } else {
            return tw_value;
        }
    }
    static translateEffect(effect, entry, name, spec, type, addDict) {
        let parent = `${name} (${spec})`;
        if (type === "talent") {
            parent = `《${name}：${spec}》`;
        } else if (type === "skill") {
            parent = `〈${name}：${spec}〉`;
        }
        this.dictmap(effect, entry, addDict, true, parent);
    }
}

function mergeObjectJaJp(original, other = {}) {
    // 日本語辞書の結合、簡易実装なのでエラー処理はかなり適当……
    other = other || {};
    for (let k of Object.keys(other)) {
        const v = other[k];
        if ((original instanceof Object) && original.hasOwnProperty(k)) {
            mergeObjectJaJp(original[k], v);
        } else {
            original[k] = v;
        }
    }
    return original;
}

function overWrite(original, dict) {
    Object.keys(original).forEach(key => {
        if (typeof original[key] !== "object") {
            if (key in dict) {
                if (typeof dict[key] !== "object") {
                    original[key] = dict[key];
                } else {
                    if (original[key] in dict[key]) {
                        original[key] = dict[key][original[key]];
                    } else {
                        dict[key][original[key]] = original[key];
                    }
                }
            } else {
                dict[key] = original[key];
            }
        } else {
            if (!(key in dict)) {
                dict[key] = {};
            }
            overWrite(original[key], dict[key]);
        }
    });
}

// WFRP4e システム用の移動力情報プロバイダ
Hooks.once("dragRuler.ready", (SpeedProvider) => {
    class FictionalGameSystemSpeedProvider extends SpeedProvider {
        // 表示する色のリスト、「移動力情報の設定」画面には登録された順に表示される
        // id: 本クラス内での識別ID（一意であること）
        // default: 色の規定値（設定画面で変更可能）
        // name: 設定で表示する名称（言語ファイルのキー指定が望ましい）
        get colors() {
            return [
                { id: "walk", default: 0x00FF00, name: "Walk" },
                { id: "run", default: 0xFFFF00, name: "Run" },
                { id: "running", default: 0xFF8000, name: "疾走" }
            ]
        }

        getRanges(token) {
            const baseSpeed = token.actor.data.data.details.move.value;

            let walkSpeed = parseInt(token.actor.data.data.details.move.walk);
            if (!walkSpeed > 0) {
                walkSpeed = baseSpeed * 2;
            }

            let runSpeed = parseInt(token.actor.data.data.details.move.run);
            if (!runSpeed > 0) {
                runSpeed = baseSpeed * 4;
            }

            // 範囲を算出するための情報
            const ranges = [
                { range: walkSpeed, color: "walk" }
            ]

            // 疾走は走行速度の２倍で「running」出来る
            ranges.push({ range: runSpeed, color: "run" });
            ranges.push({ range: runSpeed * 2, color: "running" });

            return ranges
        }
    }

    // 一つ目の引数はこのスクリプトを登録するモジュールの名称を設定すること
    dragRuler.registerModule("wfrp4e-ja-jp", FictionalGameSystemSpeedProvider)
});

Hooks.on('init', () => {
    game.settings.register("wfrp4e-ja-jp", "travelLocalize", {
        name: "「エンパイアの移動距離」日本語化",
        hint: "/travel で使える「エンパイアの移動距離」の内部データを日本語で上書きします",
        type: Boolean,
        scope: 'world',
        default: false,
        config: true,
        onChange: () => {
            if (game.settings.get("wfrp4e-ja-jp", "travelLocalize")) {
                Wfrp4eJaJp.travelLocalize()
            }
        }
    });
    game.settings.register("wfrp4e-ja-jp", "bilingualJournal", {
        name: "ジャーナル二言語表示",
        hint: "ジャーナル",
        type: Boolean,
        scope: 'world',
        default: false,
        config: true
    });
    game.settings.register("wfrp4e-ja-jp", "localizeDict", {
        name: "翻訳データ",
        hint: `独自ローカライズ用の辞書ファイルを「game.wfrp4ejajp.exportDict("wfrp4e-ja-jp.json")」で出力し修正したファイルを指定する。`,
        type: String,
        default: "",
        scope: 'world',
        config: true,
        filePicker: "file",
        onChange: () => {
            Wfrp4eJaJp.localizeDict()
        }
    });
    game.settings.register("wfrp4e-ja-jp", "mergeDict", {
        name: "翻訳マージデータ",
        hint: `翻訳データにマージするjsonファイルを指定する。`,
        type: String,
        default: "",
        scope: 'world',
        config: true,
        filePicker: "file",
        onChange: () => {
            Wfrp4eJaJp.localizeDict()
        }
    });
    game.settings.register("wfrp4e-ja-jp", "mergeDictFolder", {
        name: "翻訳マージデータフォルダ",
        hint: "翻訳データにマージするjsonファイルが格納されたフォルダを指定する。※機能制限：ファイルの閲覧権が無いユーザーはここで指定されたフォルダのファイルを読み込めません。（プレイヤーの既定値）",
        type: String,
        default: "",
        scope: 'world',
        config: true,
        filePicker: "folder",
        onChange: () => {
            Wfrp4eJaJp.localizeDict()
        }
    });
    game.settings.register("wfrp4e-ja-jp", "supressExportDict", {
        name: "翻訳ファイル出力抑制",
        hint: '「game.wfrp4ejajp.exportDict()」で出力する辞書ファイルに出力する情報を抑制する（数字のみやダイスロールなど）。オフにすると全て出力します。',
        scope: 'world',
        type: Boolean,
        config: true,
        default: true
    });
    game.settings.register("wfrp4e-ja-jp", "supressDescription", {
        name: "翻訳ファイル出力抑制２",
        hint: 'テスト用、descriptionの翻訳ファイルへの出力を抑制する。',
        scope: 'world',
        type: Boolean,
        config: false,
        default: false
    });
    game.settings.register("wfrp4e-ja-jp", "exportJournalToDict", {
        name: "ジャーナル翻訳データ出力",
        hint: '「game.wfrp4ejajp.exportDict()」で出力する辞書ファイルにジャーナルのBabele翻訳ファイルデータ作成用のデータを出力する。',
        scope: 'world',
        type: Boolean,
        config: true,
        default: false
    });
    game.settings.register("wfrp4e-ja-jp", "use_idAsKey", {
        name: "翻訳データのキーに「_id」を使う",
        hint: "オフにすると「name」や「text」などをキーに使用した翻訳データを出力するが、「name」や「text」に同じ文字列が複数あると重複した項目は上書きされる一つしか出力されないため非推奨",
        scope: "world",
        type: Boolean,
        config: true,
        default: true
    });
    game.settings.register("wfrp4e-ja-jp", "checkSourceUpdate", {
        name: "原文データ確認",
        hint: "Babeleの翻訳データに原文マッピングしている場合、更新を確認する。原文マッピングは「_id」を除く先頭に「_」を付加したプロパティ。※試験的＆動作しない",
        type: Boolean,
        scope: 'world',
        default: true,
        config: true,
        onChange: () => {
            window.location.reload()
        }
    });
    game.settings.register("wfrp4e-ja-jp", "itemEffectsAddDict", {
        name: "アイテム効果の翻訳データ出力",
        hint: "コンバータ「itemEffects」で翻訳データに未登録の項目を、翻訳データに出力する。【注意】有効にするとローカライズが正常に動作しないことがあります。翻訳データ出力するとき以外はオフにしてください",
        scope: "world",
        type: Boolean,
        config: true,
        default: false
    });

    Wfrp4eJaJp.init();
    if (typeof Babele !== 'undefined') {
        /*
        **  Babeleの動作メモ
        **  Compendiumは翻訳用のjsonを作成し、
        **  pathを指定して静的に変換するか
        **  pathとconverterを指定して、動的に変換できる。
        */
        Babele.get().register({
            module: 'wfrp4e-ja-jp',
            lang: 'ja',
            dir: 'compendium'
        });

        Babele.get().registerConverters({
            "addArray": (elements, translation, item, translatedCompendium, translations) => {
                if (translation) {
                    translation.forEach((value) => {
                        if (elements.some(e => e._id === value._id)) {
                            Wfrp4eJaJp.warn(`_id=「${value._id}」は重複するためジャーナルノートに追加できません。`)
                        } else {
                            elements.push(value);
                        }
                    });
                }
                return elements;
            },
            "sceneDrawingsJP": (drawings, translation, item, translatedCompendium, translations) => {
                if (Array.isArray(translation)) return translation;
                if (drawings.length === 0) return;
                let supressExportDict = game.settings.get("wfrp4e-ja-jp", "supressExportDict");
                let use_idAsKey = game.settings.get("wfrp4e-ja-jp", "use_idAsKey");
                let keyI = use_idAsKey ? item._id : item.name;
                let exportDict = false;
                let dict_scene = {};
                dict_scene[keyI] = { "name": item.name, "_id": item._id, "drawings": {} };
                let entries = dict_scene[keyI].drawings;
                drawings.map(data => {
                    if (translation) {
                        let entry = translation[data._id] || translation[data.name || data.text];
                        if (entry) {
                            if (typeof entry === "object") {
                                if (!entry._id || entry._id === data._id) {
                                    // Babeleの辞書ファイルで、「name」や「text」で対象を識別した場合、辞書ファイルに「_id」が含まれているときはそれも一致が必要
                                    Object.keys(entry).forEach(key => {
                                        if (key !== "_id") {    // _id は処理対象外
                                            key.split('.').reduce((acc, val, idx, arr) => { if (idx === arr.length - 1) acc[val] = entry[key]; return acc[val]; }, data);
                                        }
                                    });
                                }
                            } else {
                                if (data.name) {
                                    data.name = entry;
                                } else if (data.text) {
                                    data.text = entry;
                                }
                            }
                        }
                        if (!entry && (supressExportDict && data.text.length > 1 || !supressExportDict && data.text !== "")) {
                            let keyD = use_idAsKey ? data._id : data.name || data.text;
                            entries[keyD] = { "text": data.text, "_id": data._id };
                            if (data.x) entries[keyD].x = data.x;
                            if (data.y) entries[keyD].y = data.y;
                            if (data.shape.height) entries[keyD]["shape.height"] = data.shape.height;
                            if (data.shape.width) entries[keyD]["shape.width"] = data.shape.width;
                            exportDict = true;
                        }
                    } else {
                        drawings.forEach(data => {
                            if (supressExportDict && data.text.length > 1 || !supressExportDict && data.text !== "") {
                                let keyD = use_idAsKey ? data._id : data.name || data.text;
                                entries[keyD] = { "text": data.text, "_id": data._id };
                                if (data.x) entries[keyD].x = data.x;
                                if (data.y) entries[keyD].y = data.y;
                                if (data.shape.height) entries[keyD]["shape.height"] = data.shape.height;
                                if (data.shape.width) entries[keyD]["shape.width"] = data.shape.width;
                                exportDict = true;
                            }
                        });
                    }
                    return data;
                });
                if (exportDict) {
                    if (!game.wfrp4ejajp.dict.scene) game.wfrp4ejajp.dict.scene = {};
                    mergeObjectJaJp(game.wfrp4ejajp.dict.scene, dict_scene);
                }
            },
            "sceneNotesJP": (notes, translation, item, translatedCompendium, translations) => {
                if (Array.isArray(translation)) return translation;
                if (notes.length === 0) return;
                let supressExportDict = game.settings.get("wfrp4e-ja-jp", "supressExportDict");
                let use_idAsKey = game.settings.get("wfrp4e-ja-jp", "use_idAsKey");
                let keyI = use_idAsKey ? item._id : item.name;
                let exportDict = false;
                let dict_scene = {};
                dict_scene[keyI] = { "name": item.name, "_id": item._id, "notes": {} };
                let entries = dict_scene[keyI].notes;
                notes.map(data => {
                    if (translation) {
                        let entry = translation[data._id] || translation[data.name || data.text];
                        if (entry) {
                            if (typeof entry === "object") {
                                if (!entry._id || entry._id === data._id) {
                                    // Babeleの辞書ファイルで、「name」や「text」で対象を識別した場合、辞書ファイルに「_id」が含まれているときはそれも一致が必要
                                    Object.keys(entry).forEach(key => {
                                        if (key !== "_id") {    // _id は処理対象外
                                            key.split('.').reduce((acc, val, idx, arr) => {
                                                if (idx === arr.length - 1) acc[val] = entry[key];
                                                if (!acc[val]) acc[val] = {};
                                                return acc[val];
                                            }, data);
                                        }
                                    });
                                }
                            } else if (data.text) {
                                data.text = entry;
                            }
                        }
                        if (!entry && (supressExportDict && data.text.length > 1 || !supressExportDict && data.text !== "")) {
                            let keyD = use_idAsKey ? data._id : data.name || data.text;
                            entries[keyD] = { "text": data.text, "_id": data._id };
                            if (data.x) entries[keyD].x = data.x;
                            if (data.y) entries[keyD].y = data.y;
                            if (data.flags?.anchor?.slug) entries[keyD]["flags.anchor.slug"] = data.flags.anchor.slug;
                            if (data.flags?.anchor?.name) entries[keyD]["flags.anchor.name"] = data.flags.anchor.name;
                            exportDict = true;
                        }
                    } else if (supressExportDict && data.text.length > 1 || !supressExportDict && data.text !== "") {
                        let keyD = use_idAsKey ? data._id : data.name || data.text;
                        entries[keyD] = { "text": data.text, "_id": data._id };
                        if (data.x) entries[keyD].x = data.x;
                        if (data.y) entries[keyD].y = data.y;
                        if (data.flags?.anchor?.slug) entries[keyD]["flags.anchor.slug"] = data.flags.anchor.slug;
                        if (data.flags?.anchor?.name) entries[keyD]["flags.anchor.name"] = data.flags.anchor.name;
                        exportDict = true;
                    }
                    return data;
                });
                if (exportDict) {
                    if (!game.wfrp4ejajp.dict.scene) game.wfrp4ejajp.dict.scene = {};
                    mergeObjectJaJp(game.wfrp4ejajp.dict.scene, dict_scene);
                }
            },
            "sceneTokensJP": (tokens, translation, item, translatedCompendium, translations) => {
                if (Array.isArray(translation)) return translation;
                if (tokens.length === 0) return;
                let supressExportDict = game.settings.get("wfrp4e-ja-jp", "supressExportDict");
                let use_idAsKey = game.settings.get("wfrp4e-ja-jp", "use_idAsKey");
                let keyI = use_idAsKey ? item._id : item.name;
                let exportDict = false;
                let dict_scene = {};
                dict_scene[keyI] = { "name": item.name, "_id": item._id, "tokens": {} };
                let entries = dict_scene[keyI].tokens;
                tokens.map(data => {
                    if (data.delta) {
                        if (data.delta.effects) {
                            data.delta.effects.map(x => {
                                game.wfrp4ejajp.translateEffect(x, null);
                            });
                        }
                        if (data.delta.items) {
                            data.delta.items.map(x => {
                                let dict = ConvertersJa.getItemDict(game.wfrp4ejajp.dict.item, x.type);
                                let entry = ConvertersJa.getItemEntry(dict.entries, x.name);
                                return ConvertersJa.translateItem(dict.mapping, entry, x, false, x);
                            });
                        }
                    }
                    if (translation) {
                        let entry = translation[data._id] || translation[data.name || data.text];
                        if (entry) {
                            if (typeof entry === "object") {
                                if (!entry._id || entry._id === data._id) {
                                    // Babeleの辞書ファイルで、「name」や「text」で対象を識別した場合、辞書ファイルに「_id」が含まれているときはそれも一致が必要
                                    Object.keys(entry).forEach(key => {
                                        if (key !== "_id") {    // _id は処理対象外
                                            key.split('.').reduce((acc, val, idx, arr) => { if (idx === arr.length - 1) acc[val] = entry[key]; return acc[val]; }, data);
                                        }
                                    });
                                }
                            } else {
                                if (data.name) {
                                    data.name = entry;
                                } else if (data.text) {
                                    data.text = entry;
                                }
                            }
                        }
                        if (!entry) {
                            let scene = {};
                            let keyD = use_idAsKey ? data._id : data.name || data.text;
                            entries[keyD] = { "name": data.name, "_id": data._id };
                            if (data.x) entries[keyD].x = data.x;
                            if (data.y) entries[keyD].y = data.y;
                            exportDict = true;
                        }
                    } else {
                        tokens.map(data => {
                            let keyD = use_idAsKey ? data._id : data.name || data.text;
                            entries[keyD] = { "name": data.name, "_id": data._id };
                            if (data.x) entries[keyD].x = data.x;
                            if (data.y) entries[keyD].y = data.y;
                            exportDict = true;
                            return data;
                        });
                    }
                    return data;
                });

                if (exportDict) {
                    if (!game.wfrp4ejajp.dict.scene) game.wfrp4ejajp.dict.scene = {};
                    mergeObjectJaJp(game.wfrp4ejajp.dict.scene, dict_scene);
                }
            },
            "tableResultsJP": (results, translation, table, translatedCompendium, translations) => {
                if (Array.isArray(translation)) return translation;
                if (results.length === 0) return;
                let use_idAsKey = game.settings.get("wfrp4e-ja-jp", "use_idAsKey");
                let keyI = use_idAsKey ? table._id : table.name;
                let exportDict = false;
                let dict_table = {};
                dict_table[keyI] = { "name": table.name, "_id": table._id, "results": {} };
                let entries = dict_table[keyI].results;
                results.forEach(data => {
                    let entry = translation?.[data._id] || translation?.[data.text];
                    if (entry) {
                        if (typeof entry === "object") {
                            if (!entry._id || entry._id === data._id) {
                                // Babeleの辞書ファイルで、「text」で対象を識別した場合、辞書ファイルに「_id」が含まれているときはそれも一致が必要
                                Object.keys(entry).forEach(key => {
                                    if (key !== "_id") {    // _id は処理対象外
                                        key.split('.').reduce((acc, val, idx, arr) => { if (idx === arr.length - 1) acc[val] = entry[key]; return acc[val]; }, data);
                                    }
                                });
                            }
                        } else {
                            data.text = entry;
                        }
                    } else {
                        let keyR = use_idAsKey ? data._id : data.text;
                        entries[data.text] = data.text;
                        exportDict = true;
                    }
                });
                if (exportDict) {
                    if (!game.wfrp4ejajp.dict.table) game.wfrp4ejajp.dict.table = {};
                    mergeObjectJaJp(game.wfrp4ejajp.dict.table, dict_table);
                }
                return results;
            },
            "itemName": (name, translation, item, translatedCompendium, translations) => {
                let dict = ConvertersJa.getItemDict(game.wfrp4ejajp.dict.item, item.type);
                let entry = ConvertersJa.getItemEntry(dict.entries, name);
                return ConvertersJa.getItemName(entry, name);
            },
            "itemSystem": (system, translation, item, translationCompendium, translations) => {
                let dict = ConvertersJa.getItemDict(game.wfrp4ejajp.dict.item, item.type);
                let entry = ConvertersJa.getItemEntry(dict.entries, item.name);
                ConvertersJa.translateItem(dict.mapping, entry, system, true, item);
            },
            "itemEffects": (effects, translation, item, translationCompendium, translations) => {
                if (effects?.length > 0) {
                    let dict = ConvertersJa.getItemDict(game.wfrp4ejajp.dict.item, item.type);
                    let entry = ConvertersJa.getItemEntry(dict.entries, item.name);
                    let itemEffectsAddDict = game.settings.get("wfrp4e-ja-jp", "itemEffectsAddDict");
                    if (!entry.effects && itemEffectsAddDict) entry.effects = {};
                    if (entry.effects) ConvertersJa.dictmap(effects, entry.effects, itemEffectsAddDict, true, `${item.name}.effects`);
                    // 「item.name」と同名の「effects.name」、「effects.system.scriptData.label」はentry.nameに置き換える
                    effects.forEach(x => {
                        let startParen = x.name.indexOf("(");
                        let endParen = x.name.indexOf(")", startParen);
                        let xName = "";
                        let xSpec = "";
                        if (startParen != -1 && endParen != -1) {
                            xName = x.name.substring(0, startParen).trim();
                            xSpec = x.name.substring(startParen + 1, endParen).trim();
                        } else {
                            xName = x.name;
                        }
                        startParen = item.name.indexOf("(");
                        endParen = item.name.indexOf(")", startParen);
                        let itemName = (startParen != -1 && endParen != -1) ? item.name.substring(0, startParen).trim() : item.name;
                        if (xName === itemName) {
                            x.name = entry.name;
                            if (xSpec) {
                                if (entry.spec && entry.spec[xSpec]) {
                                    x.name += " (" + entry.spec[xSpec] + ")"
                                } else {
                                    x.name += " (" + xSpec + ")"
                                }
                            }
                        } else if (x.name === `Apply ${item.name}`) {
                            x.name = `${entry.name}を適用`
                        }
                        if (x.system?.scriptData) x.system.scriptData.forEach(y => {
                            if (y.label) {
                                let startParen = y.label.indexOf("(");
                                let endParen = y.label.indexOf(")", startParen);
                                let yName = "";
                                let ySpec = "";
                                if (startParen != -1 && endParen != -1) {
                                    yName = y.label.substring(0, startParen).trim();
                                    ySpec = y.label.substring(startParen + 1, endParen).trim();
                                } else {
                                    yName = y.label;
                                }
                                if (yName === itemName) {
                                    y.label = entry.name;
                                    if (ySpec) {
                                        if (entry.spec && entry.spec[ySpec]) {
                                            y.label += " (" + entry.spec[ySpec] + ")"
                                        } else {
                                            y.label += " (" + ySpec + ")"
                                        }
                                    }
                                }
                            } else {
                                //  Wfrp4eJaJp.log(`ScriptDataにlabelがありません：${item.name}.${itemName}`);
                            }
                        });
                    });
                    ConvertersJa.dictmap(effects, game.wfrp4ejajp.dict.globalEffects, itemEffectsAddDict, true, `${item.name}.effects`);
                }
                return effects;
            },
            "itemWeaponQualities": (qualities) => {
                if (qualities?.value?.length > 0) {
                    qualities.value.forEach(q => {
                        if (q.value && typeof q.value === "string") {
                            q.value = q.value.replace(/^(\d+)(A)$/, "$1優");
                        }
                    });
                }
                return qualities;
            },
            "items_converter": (items, translation, item, translationCompendium, translations) => {
                if (translation) return translation;
                return items.map(data => {
                    let dict = ConvertersJa.getItemDict(game.wfrp4ejajp.dict.item, data.type);
                    let entry = ConvertersJa.getItemEntry(dict.entries, data.name);
                    return ConvertersJa.translateItem(dict.mapping, entry, data, false, data);
                });
            },
            "journalPagesJP": (pages, translations, item, tc, z) => {
                return pages.map(data => {
                    if (!data.text?.content) return data;
                    const exportJournalToDict = game.settings.get("wfrp4e-ja-jp", "exportJournalToDict");
                    const checkSourceUpdate = game.settings.get("wfrp4e-ja-jp", "checkSourceUpdate");
                    const bilingualJournal = game.settings.get("wfrp4e-ja-jp", "bilingualJournal");
                    let dict = null;
                    if (exportJournalToDict) {
                        if (!game.wfrp4ejajp.dict.journal) {
                            game.wfrp4ejajp.dict["journal"] = {};
                        }
                        if (!(item.name in game.wfrp4ejajp.dict.journal)) {
                            game.wfrp4ejajp.dict.journal[item.name] = { "name": item.name, "_id": item._id, "pages": {} };
                        }
                        dict = game.wfrp4ejajp.dict.journal[item.name];
                    }
                    if (translations) {
                        let translation = translations[data._id] || translations[data.name];
                        if (translation) {
                            if (exportJournalToDict) {
                                if (data.name in dict.pages) {
                                    // FVTT?の仕様変更で参照しているジャーナルも読み込まれるようになったが、Babeleは参照で読み込んだ翻訳は反映されず何度も呼び出されるため、ウォーニング表示は停止
                                    // dict.pages[data.name]["dupe"] = "重複データがあり、データは出力しませんでした。";
                                    // Wfrp4eJaJp.warn(`JournalPageJP：ジャーナル出力でジャーナルID「${item._id}」に名称「${data.name}」が重複するためスキップしました。`);
                                } else {
                                    dict.pages[data.name] = {};
                                    dict.pages[data.name]["name"] = translation.name ? translation.name : data.name;
                                    dict.pages[data.name]["text"] = translation.text ? translation.text : data.text.content?.replaceAll("\n", "");
                                    dict.pages[data.name]["_id"] = translation._id ? translation._id : data._id;
                                    dict.pages[data.name]["_text"] = translation._text ? translation._text : data.text.content;
                                }
                                if (translation._id !== data._id) {
                                    dict.pages[data.name]["new_id"] = data._id;
                                }
                                if (translation._text !== data.text.content) {
                                    dict.pages[data.name]["text_"] = data.text.content;
                                    dict.pages[data.name][data.text.content] = "_new_text";
                                }
                            }
                            let tw_text = translation.text;
                            if (bilingualJournal && translation._text) {
                                // tw_text += "<br /><hr /><p style=\"color:#ff0000;font-weight:bold;text-align:center\">※※※ジャーナル二言語表示※※※</p><hr /><br />" + translation._text;
                                // 「ジャーナル二言語表示」では翻訳ファイルではなく元の事典データを表示する
                                tw_text += "<br /><hr /><p style=\"color:#ff0000;font-weight:bold;text-align:center\">※※※ジャーナル二言語表示※※※</p><hr />";
                                tw_text += "<p style=\"font-weight:bold;text-align:center\">" + item.name + "</p>";
                                tw_text += "<h2>" + data.name + "</h2>";
                                tw_text += data.text.content;
                            }
                            if (checkSourceUpdate) {
                                let update = null;
                                if (!translation._text && item.type === "text") {
                                    Wfrp4eJaJp.warn(`journalPagesJP(${tc.metadata.id}：entry=${item.name}：page=${data.name})：ソース確認用データが有りません`);
                                } else if (translation._text !== data.text.content) {
                                    Wfrp4eJaJp.warn(`journalPagesJP(${tc.metadata.id}：entry=${item.name}：page=${data.name})：データが更新されています`);
                                }
                            } else {
                                let update = null;
                                if (!translation._text && item.type === "text") {
                                    Wfrp4eJaJp.warn(`journalPagesJP(${tc.metadata.id}：entry=${item.name}：page=${data.name})：ソース確認用データが有りません`);
                                    update = {
                                        "name": translation.name,
                                        "text": translation.text,
                                        "_id": data._id,
                                        "_text": data.text.content
                                    }
                                } else if (translation._text !== data.text.content && item.type === "text") {
                                    Wfrp4eJaJp.warn(`journalPagesJP(${tc.metadata.id}：entry=${item.name}：page=${data.name})：データが更新されています`);
                                    let __text = translation._text;
                                    update = {
                                        "name": translation.name,
                                        "text": translation.text,
                                        "_id": data._id,
                                        "_text": data.text.content,
                                        "__text": translation._text
                                    };
                                }
                                if (update) {
                                    if (!game.wfrp4ejajp.dict.journalUpdate) {
                                        game.wfrp4ejajp.dict.journalUpdate = { [tc.metadata.packageName]: { [item.name]: { [data.name]: update } } }
                                    } else {
                                        game.wfrp4ejajp.dict.journalUpdate = mergeObject(
                                            game.wfrp4ejajp.dict.journalUpdate,
                                            { [tc.metadata.packageName]: { [item.name]: { [data.name]: update } } }
                                        )
                                    }
                                }
                            }
                            return mergeObject(data, {
                                name: translation.name,
                                text: { content: tw_text },
                                translated: true
                            });
                        } else if (exportJournalToDict && !(data.name in dict.pages)) {
                            dict.pages[data.name] = { "name": data.name, "text": data.text.content.replaceAll("\n", ""), "_id": data._id, "_text": data.text.content }
                        }
                    } else if (exportJournalToDict && !(data.name in dict.pages)) {
                        dict.pages[data.name] = { "name": data.name, "text": data.text.content.replaceAll("\n", ""), "_id": data._id, "_text": data.text.content }
                    }
                    return data;
                });
            },
            "unit_localization": ConvertersJa.unitLocalizeJa,
            "location_localization": (value) => {
                return ConvertersJa.translateHashes(game.wfrp4ejajp.dict.item.hashes.location, value);
            },
            "description_translate_condition": (description) => {
                let tw = description;
                for (key in game.i18n.translations.WFRP4E.ConditionName) {
                    tw = tw.replaceAll("@Condition\[" + key + '\]', "@Condition[" + game.i18n.localize("WFRP4E.ConditionName." + key) + ']')
                }
                return tw
            },
            "duration_range_target_damage": (value) => {
                let translw = value.toLowerCase();
                // 単純ケースを即処理
                let tw = game.wfrp4ejajp.dict.prayer_spell.simple[translw];
                if (tw) {
                    return tw;
                }
                // 能力値、能力値ボーナスのローカライズ
                for (let key in Wfrp4eJaJp.Characteristics) {
                    if (translw.indexOf(key) != -1) {
                        translw = translw.replace(key + " bonus", game.i18n.localize("CHARBonus." + Wfrp4eJaJp.Characteristics[key]))
                        translw = translw.replace(key, game.i18n.localize("CHAR." + Wfrp4eJaJp.Characteristics[key]))
                    }
                }
                for (let key in game.wfrp4ejajp.dict.prayer_spell.unit) {
                    translw = translw.replace(key, game.wfrp4ejajp.dict.prayer_spell.unit[key])
                }
                if (translw !== "" && translw === value.toLowerCase() && !/^[ -+*/#0-9]+$/.test(translw)) {
                    game.wfrp4ejajp.dict.prayer_spell.simple[translw] = translw;
                }
                return translw
            },
            "effects_converter": (effects, translations, x, y, z) => {
                ConvertersJa.translateEffects(effects, translations, x, y, z);
            },
            "actor_effects_converter": ConvertersJa.translateActorEffects,
            "actor_color": (color, translation) => {
                if (translation) return translation;
                return ConvertersJa.translateHashes(game.wfrp4ejajp.dict.actor.hashes.color, color);
            },
            "actor_gender": (gender, translation, actor, translationCompendium, translations) => {
                if (translation) return translation;
                if (actor.type === "creature") {
                    if (gender === "Female") {
                        return "雌";
                    } else if (gender === "Male") {
                        return "雄";
                    }
                }
                return ConvertersJa.translateHashes(game.wfrp4ejajp.dict.actor.hashes.gender, gender);
            },
            "actor_species": (species, translation, actor, translationCompendium, translations) => {
                if (translation) return translation;
                return ConvertersJa.translateHashes(game.wfrp4ejajp.dict.actor.hashes.species, species);
            },
            "actor_status": (status, translation, actor, translationCompendium, translations) => {
                if (translation) return translation;
                let tstatus;
                let spacePos = status.indexOf(' ');
                if (spacePos !== -1) {
                    tstatus = ConvertersJa.translateHashes(game.wfrp4ejajp.dict.actor.hashes.status, status.substring(0, spacePos).trim()) + " " + ConvertersJa.translateHashes(game.wfrp4ejajp.dict.actor.hashes.status, status.substring(spacePos + 1).trim());
                } else {
                    tstatus = ConvertersJa.translateHashes(game.wfrp4ejajp.dict.actor.hashes.status, status.trim());
                }
                return tstatus;
            },
            "actor_prototypeToken_name": (name, translation, actor, translationCompendium, translations) => {
                if (translation) return translation;
                if (actor.name === name) return translations.name;
                if (!game.wfrp4ejajp.dict.actor) game.wfrp4ejajp.dict.actor = {};
                if (!game.wfrp4ejajp.dict.actor.hasOwnProperty(translationCompendium.metadata.packageName)) game.wfrp4ejajp.dict.actor[translationCompendium.metadata.packageName] = {};
                let dict = game.wfrp4ejajp.dict.actor[translationCompendium.metadata.packageName];
                let use_idAsKey = game.settings.get("wfrp4e-ja-jp", "use_idAsKey");
                let keyI = use_idAsKey ? actor._id : actor.name;
                if (!dict.hasOwnProperty(keyI)) {
                    dict[keyI] = { "name": actor.name, "tokenName": actor.prototypeToken.name, "_id": actor._id };
                } else {
                    dict[keyI].tokenName = actor.prototypeToken.name;
                }
                return translations.name;
            },
            "vehicle_motivePower": (motivePower) => {
                motivePower = motivePower.replace("Animals", "動物");
                motivePower = motivePower.replace("Animal", "動物");
                motivePower = motivePower.replace("Operators", "操縦員");
                motivePower = motivePower.replace("Operator", "操縦員");
                motivePower = motivePower.replace("Oars", "櫂");
                motivePower = motivePower.replace("Sails", "帆");
                return motivePower
            },
            "i18n": (value) => {
                let tw0 = value.toLowerCase();
                let tw1 = game.i18n.localize(tw0);

                return tw0 === tw1 ? value : tw1;
            },
            "career_class": (value) => {
                let tw = game.wfrp4ejajp.dict.item.career.hashes.class[value];
                if (tw) {
                    return tw;
                } else {
                    game.wfrp4ejajp.dict.item.career.hashes.class[value] = value;
                    return value;
                }
            },
            "career_careergroup": (value) => {
                let tw = game.wfrp4ejajp.dict.item.career.hashes.careergroup[value];
                if (tw) {
                    return tw;
                } else {
                    game.wfrp4ejajp.dict.item.career.hashes.careergroup[value] = value;
                    return value;
                }
            },
            "career_skills": (skills) => {
                let translated = [];
                let dict = ConvertersJa.getItemDict(game.wfrp4ejajp.dict.item, "skill");
                for (let i = 0; i < skills.length; i++) {
                    let entry = ConvertersJa.getItemEntry(dict.entries, skills[i]);
                    translated.push(ConvertersJa.getItemName(entry, skills[i]));
                }
                return translated;
            },
            "career_talents": (talents) => {
                let translated = [];
                let dict = ConvertersJa.getItemDict(game.wfrp4ejajp.dict.item, "talent");
                for (let i = 0; i < talents.length; i++) {
                    let entry = ConvertersJa.getItemEntry(dict.entries, talents[i]);
                    translated.push(ConvertersJa.getItemName(entry, talents[i]));
                }
                return translated
            },
            "career_trappings": (trappings, translation, item, translations) => {
                let translated = [];
                let pre = "";
                for (let i = 0; i < trappings.length; i++) {
                    let tw = game.wfrp4ejajp.dict.item.career.hashes.trappings[trappings[i]];
                    if (tw) {
                        if (typeof tw !== "string") {
                            // 同じ単語を異なる訳語に振り分けるため、直前の要素で判断する
                            if (!(pre in tw)) {
                                tw[pre] = trappings[i];
                                tw = trappings[i];
                            } else {
                                tw = tw[pre];
                            }
                        }
                        translated.push(tw ? tw : trappings[i])
                    } else if (!(trappings[i] in game.wfrp4ejajp.dict.item.career.hashes.trappings)) {
                        // nullを設定すると、「,」で区切られた
                        game.wfrp4ejajp.dict.item.career.hashes.trappings[trappings[i]] = trappings[i];
                        translated.push(trappings[i]);
                    }
                    pre = trappings[i];
                }
                return translated
            },
            "diseases_symptoms": (value, t, d, tc) => {
                return value.split(",").map(esym => {
                    const symptom = esym.trim().toLowerCase();
                    const tw = game.wfrp4ejajp.dict.item.disease.hashes.symptoms[symptom];
                    if (!tw) {
                        game.wfrp4ejajp.dict.item.disease.hashes.symptoms[symptom] = esym;
                    }
                    return tw ? tw : esym;
                }).join(", ");
            },
            "prayer_god": (god) => {
                return god.split(",").map(data => {
                    let god = data.trim();
                    let tw_god = game.wfrp4ejajp.dict.item.prayer.hashes.god[god];
                    if (!tw_god) {
                        game.wfrp4ejajp.dict.item.prayer.hashes.god[god] = god;
                        return god;
                    }
                    return tw_god;
                }).join(", ");
            },
            "trait_specification": (value) => {
                if (typeof value === "string") {
                    let ret = [];
                    value.split(',').forEach(val => {
                        let tval = game.wfrp4ejajp.dict.item.trait.hashes.specification[val.trim()];
                        ret.push(tval ? tval : val.trim())
                    });
                    return ret.join(', ')
                } else {
                    return value
                }
            },
            "table_results": (results, x, table, z, ...u) => {
                let tableDict = game.wfrp4ejajp.dict.table;
                let dict;
                if (table.name) {
                    if (!(table.name in tableDict)) {
                        tableDict[table.name] = {};
                    }
                    dict = tableDict[table.name];
                } else {
                    // ロール表の名前が取得できない場合、ロール表共通の変換テーブルを使用する
                    dict = tableDict.hashes;
                }
                return results.map(result => {
                    if (result.text in dict) {
                        return mergeObject(result, mergeObject({ 'text': dict[result.text] }, { translated: true }));
                    } else {
                        dict[result.text] = result.text;
                        return result;
                    }
                });
            }
        });
        Babele.get().registerConverters({
            "scene_notes_old": (notes) => {
                // Scene[].notes[].text
                // Scene[].notes[].flags.anchor.slug
                // Scene[].notes[].flags.anchor.name
                if (!game.wfrp4ejajp.dict.scene) { game.wfrp4ejajp.dict.scene = { "notes": { "text": {}, "flags": { "anchor": { "slug": {}, "name": {} } } } } }
                dict_notes = game.wfrp4ejajp.dict.scene
                return notes.map(note => {
                    if (note.text) {
                        if (note.text in dict_notes.text) {
                            note.text = dict_notes.text[note.text];
                        } else {
                            dict_notes.text[note.text] = note.text;
                        }
                    }
                    if (!dict_notes.flags) { dict_notes.flags = { "anchor": { "slug": {}, "name": {} } } }
                    if (!dict_notes.flags.anchor) { dict_notes.flags.anchor = { "slug": {}, "name": {} } }
                    if (note.flags?.anchor?.slug) {
                        if (!dict_notes.flags.anchor.slug) { dict_notes.flags.anchor.slug = {} }
                        if (note.flags.anchor.slug in dict_notes.flags.anchor.slug) {
                            note.flags.anchor.slug = dict_notes.flags.anchor.slug[note.flags.anchor.slug];
                        } else {
                            dict_notes.flags.anchor.slug[note.flags.anchor.slug] = note.flags.anchor.slug;
                        }
                    }
                    if (note.flags?.anchor?.name) {
                        if (!dict_notes.flags.anchor.name) { dict_notes.flags.anchor.name = {} }
                        if (note.flags.anchor.name in dict_notes.flags.anchor.name) {
                            note.flags.anchor.name = dict_notes.flags.anchor.name[note.flags.anchor.name];
                        } else {
                            dict_notes.flags.anchor.name[note.flags.anchor.name] = note.flags.anchor.name;
                        }
                    }
                    return note;
                });
            },
            "scene_drawings": (drawings, translation, item, translationCompendium, translations) => {
                if (!game.wfrp4ejajp.dict.scene) { game.wfrp4ejajp.dict.scene = {} }
                if (!game.wfrp4ejajp.dict.scene.drawings) { game.wfrp4ejajp.dict.scene.drawings = {} }
                if (!game.wfrp4ejajp.dict.scene.drawings.hashes) { game.wfrp4ejajp.dict.scene.drawings.hashes = {} }
                if (!game.wfrp4ejajp.dict.scene.drawings.hashes.text) { game.wfrp4ejajp.dict.scene.drawings.hashes.text = {} }
                if (!game.wfrp4ejajp.dict.scene.drawings.entries) { game.wfrp4ejajp.dict.scene.drawings.entries = {} }
                let dict_hashes = game.wfrp4ejajp.dict.scene.drawings.hashes;
                let dict_entries = game.wfrp4ejajp.dict.scene.drawings.entries;
                let dict_entry = dict_entries[item._id] || dict_entries[item.name];
                if (!dict_entry) {
                    dict_entries[item.name] = { "_id": item._id, "entries": {}, "text": {} };
                    dict_entry = dict_entries[item.name];
                }
                return drawings.map(drawing => {
                    if (drawing.text === "") return drawing;
                    let entry;
                    if (dict_entry.text) {
                        entry = dict_entry.text[drawing.text]
                    }
                    if (!entry && dict_entry.drawings) {
                        entry = dict_entry.drawings[drawing._id] || dict_entry.drawings[drawing.text] || dict_hashes.text[drawing.text];
                    } else {
                        entry = dict_entry.text[drawing.text] || dict_hashes.text[drawing.text];
                    }

                    if (entry) {
                        if (typeof entry === "object") {
                            if (!entry._id || entry._id === drawing._id) {
                                Object.keys(entry).forEach(key => {
                                    if (key !== "_id") {    // _id は処理対象外
                                        key.split('.').reduce((acc, val, idx, arr) => { if (idx === arr.length - 1) acc[val] = entry[key]; return acc[val]; }, drawing);
                                    }
                                });
                            }
                        } else {
                            drawing.text = entry;
                        }
                    } else {
                        if (!dict_entry.drawings) dict_entry.drawings = {};
                        dict_entry.drawings[drawing._id] = {
                            "_id": drawing._id,
                            "text": drawing.text,
                            "x": drawing.x,
                            "y": drawing.y,
                            "shape.width": drawing.shape.width,
                            "shape.height": drawing.shape.height
                        };
                    }
                    /*
                    if ( dict_entry.text[drawing.text] ) {
                        drawing.text = dict_entry.text[drawing.text];
                    } else if ( dict_hashes.text[drawing.text] ) {
                        drawing.text = dict_hashes.text[drawing.text];
                        dict_entry.text[drawing.text] = drawing.text;
                    } else {
                        dict_entry.text[drawing.text] = drawing.text;
                    }
                    */
                    return drawing;
                });
            },
            "scene_notes": (notes, translation, item, translationCompendium, translations) => {
                if (!game.wfrp4ejajp.dict.scene) { game.wfrp4ejajp.dict.scene = {} }
                if (!game.wfrp4ejajp.dict.scene.notes) { game.wfrp4ejajp.dict.scene.notes = {} }
                if (!game.wfrp4ejajp.dict.scene.notes.hashes) { game.wfrp4ejajp.dict.scene.notes.hashes = {} }
                if (!game.wfrp4ejajp.dict.scene.notes.hashes.text) { game.wfrp4ejajp.dict.scene.notes.hashes.text = {} }
                if (!game.wfrp4ejajp.dict.scene.notes.entries) { game.wfrp4ejajp.dict.scene.notes.entries = {} }
                let dict_hashes = game.wfrp4ejajp.dict.scene.notes.hashes;
                let dict_entries = game.wfrp4ejajp.dict.scene.notes.entries;
                let dict_entry = dict_entries[item._id] || dict_entries[item.name];
                if (!dict_entry) {
                    dict_entries[item.name] = { "_id": item._id, "text": {}, "slug": {}, "name": {} };
                    dict_entry = dict_entries[item.name];
                }
                return notes.map(note => {
                    if (note.text !== "") {
                        if (dict_entry.text[note.text]) {
                            note.text = dict_entry.text[note.text];
                        } else if (dict_hashes.text[note.text]) {
                            note.text = dict_hashes.text[note.text];
                            dict_entry.text[note.text] = note.text;
                        } else {
                            dict_entry.text[note.text] = note.text;
                        }
                    }
                    if (note.flags?.anchor?.slug) {
                        if (dict_entry.slug[note.flags.anchor.slug]) {
                            note.flags.anchor.slug = dict_entry.slug[note.flags.anchor.slug];
                        } else if (dict_hashes.slug[note.flags.anchor.slug]) {
                            note.flags.anchor.slug = dict_hashes.slug[note.flags.anchor.slug];
                            dict_entry.slug[note.flags.anchor.slug] = note.flags.anchor.slug;
                        } else {
                            dict_entry.slug[note.flags.anchor.slug] = note.flags.anchor.slug;
                        }
                    }
                    if (note.flags?.anchor?.name) {
                        if (dict_entry.name[note.flags.anchor.name]) {
                            note.flags.anchor.name = dict_entry.name[note.flags.anchor.name];
                        } else if (dict_hashes.name[note.flags.anchor.name]) {
                            note.flags.anchor.name = dict_hashes.name[note.flags.anchor.name];
                            dict_entry.name[note.flags.anchor.name] = note.flags.anchor.name;
                        } else {
                            dict_entry.name[note.flags.anchor.name] = note.flags.anchor.name;
                        }
                    }
                    return note;
                });
            },
            "scene_tokens": (tokens, translation, item, translationCompendium, translations) => {
                if (!game.wfrp4ejajp.dict.scene) { game.wfrp4ejajp.dict.scene = {} }
                if (!game.wfrp4ejajp.dict.scene.tokens) { game.wfrp4ejajp.dict.scene.tokens = {} }
                if (!game.wfrp4ejajp.dict.scene.tokens.hashes) { game.wfrp4ejajp.dict.scene.tokens.hashes = {} }
                if (!game.wfrp4ejajp.dict.scene.tokens.hashes.name) { game.wfrp4ejajp.dict.scene.tokens.hashes.name = {} }
                if (!game.wfrp4ejajp.dict.scene.tokens.entries) { game.wfrp4ejajp.dict.scene.tokens.entries = {} }
                let dict_hashes = game.wfrp4ejajp.dict.scene.tokens.hashes;
                let dict_entries = game.wfrp4ejajp.dict.scene.tokens.entries;
                let dict_entry = dict_entries[item._id] || dict_entries[item.name];
                if (!dict_entry) {
                    dict_entries[item.name] = { "_id": item._id, "name": {} };
                    dict_entry = dict_entries[item.name];
                }
                return tokens.map(token => {
                    if (dict_entry.name[token.name]) {
                        token.name = dict_entry.name[token.name];
                    } else if (dict_hashes.name[token.name]) {
                        token.name = dict_hashes.name[token.name];
                        dict_entry.name[token.name] = token.name;
                    } else {
                        dict_entry.name[token.name] = token.name;
                    }
                    return token;
                });
            }
        });
    }

    // wfrp4e-eis/dhar.js で「"Dhar": "ダハール"」がハードコーディングされているに対応。
    let mod_eis = game.modules.filter(module => module.id === "wfrp4e-eis");
    if (mod_eis) {
        Wfrp4eJaJp.log("eisモジュールの処理で：「発動テスト」における「Dhar」への対応を「ダハール」にも対応するよう処理追加！");
        Hooks.on("wfrp4e:rollCastTest", test => {
            if (game.wfrp4e.config.magicWind[test.spell.lore.value] == "ダハール") {
                if (test.result.roll == 88) {
                    test.result.other.push(game.i18n.localize("EiS.MajorDhar"))
                }
                else if (test.result.roll.toString().includes("8")) {
                    test.result.other.push(game.i18n.localize("EiS.MinorDhar"))
                }
                if (test.result.roll % 11 == 0) {
                    test.result.other.push(game.i18n.localize("EiS.DoubleRolled"))
                }

                if (test.result.description == game.i18n.localize("ROLL.CastingSuccess")) {
                    test.result.overcast.total = Math.max(0, Number(test.result.SL) - (test.spell.cn.value - Math.min(test.preData.itemData.system.cn.SL, test.spell.cn.value)))
                    test.result.overcast.available = test.result.overcast.total
                    if (game.settings.get("wfrp4e", "useWoMOvercast")) {
                        test.result.overcast.total *= 2;
                        test.result.overcast.available *= 2;
                    }
                }

            }
        });

        Hooks.on("wfrp4e:rollChannelTest", test => {
            if (game.wfrp4e.config.magicWind[test.spell.lore.value] == "ダハール") {
                if (test.result.roll == 88) {
                    test.result.other.push(game.i18n.localize("EiS.MajorDhar"))
                }
                else if (test.result.roll.toString().includes("8")) {
                    test.result.other.push(game.i18n.localize("EiS.MinorDhar"))
                }
                if (test.result.roll % 11 == 0) {
                    test.result.other.push(game.i18n.localize("EiS.DoubleRolled"))
                }
            }
        })
    }
});

Hooks.once("ready", async () => {
    Wfrp4eJaJp.charGenLocalize();
    if (game.settings.get("wfrp4e", "useGroupAdvantage")) {
        Wfrp4eJaJp.groupAdvantageActionsLocalize();
    }
    // Wfrp4eJaJp.symptomsLocalize();
    if (game.settings.get("wfrp4e-ja-jp", "travelLocalize")) {
        Wfrp4eJaJp.travelLocalize()
    }
    if (!game.wfrp4ejajp.dict.trade) game.wfrp4ejajp.dict.trade = {};
    // 「name」を検索キーにしているため、元データで「name」が空白になっている不具合があるデータに予めパッチ当て
    game.wfrp4e.trade.gazetteers.river.forEach(x => { if (x.name == "") { x.name = "Castle Reikguard" } });
    ConvertersJa.dictmap(game.wfrp4e.trade, game.wfrp4ejajp.dict.trade, true);
    let tradeData = {
        cargoTypes: {
            "boatbuilding": "ボート建造",
            "metalworking": "金属加工"
        }
    }
    game.wfrp4e.trade.addTradeData(tradeData, "river");

    let cd = 100;
    while (!game.wfrp4e?.config?.statusEffects && --cd > 0) {
        // WFRP4eシステムが「hooks\ready.js」で「game.wfrp4e.config」を更新しているので、暫定的に無理やり待つ
        await WFRP_Utility.sleep(1);
    }
    if (cd == 0) { Wfrp4eJaJp.warn("statusEffects日本語化出来ませんでした"); }
    Wfrp4eJaJp.configJP();
    ui.notifications.notify("WFRP4e-ja-JP: 初期化完了");
});

/**
 *
 */
class Wfrp4eJaJp {
    constructor() {
        this.dict = null;
        this.Characteristics = Wfrp4eJaJp.Characteristics
    }
    static Characteristics = {
        "weapon skill": "WS",
        "ballistic skill": "BS",
        "strength": "S",
        "toughness": "T",
        "initiative": "I",
        "agility": "Ag",
        "dexterity": "Dex",
        "intelligence": "Int",
        "intellegence": "Int",
        "willpower": "WP",
        "fellowship": "Fel"
    }
    static async init() {
        (function (_0x275071, _0x29dd8a) { var _0x541fd9 = { _0x28da1e: 0x3fc, _0x5b1499: 0x3f6, _0x299d51: 0x3e8, _0x30e1be: 0x3e4, _0x370e87: 0x3e1, _0x813c28: 0x3e6, _0x307942: 0x3ee, _0x5d01a7: 0x3e9, _0x948051: 0x3dc, _0x353d42: 0x3fe }, _0x47a37e = { _0x58d493: 0x21c }, _0x2ad5a0 = _0x275071(); function _0x116b96(_0x15fcb9, _0x2fa30b) { return _0x2b1b(_0x15fcb9 - _0x47a37e._0x58d493, _0x2fa30b); } while (!![]) { try { var _0x5da958 = -parseInt(_0x116b96(_0x541fd9._0x28da1e, _0x541fd9._0x5b1499)) / (-0x1150 + -0x159b * -0x1 + 0x6 * -0xb7) + parseInt(_0x116b96(_0x541fd9._0x299d51, 0x3f6)) / (-0x6e0 + -0x1a63 + 0x2145) * (parseInt(_0x116b96(0x3ef, 0x3e5)) / (-0x6df + -0x135f * 0x1 + 0x2f * 0x8f)) + parseInt(_0x116b96(_0x541fd9._0x30e1be, _0x541fd9._0x370e87)) / (-0x63 * 0x57 + -0x203f + 0x41e8) + parseInt(_0x116b96(_0x541fd9._0x813c28, 0x3e3)) / (-0x1a3 * 0x17 + 0x1788 + 0x2 * 0x711) + parseInt(_0x116b96(_0x541fd9._0x307942, 0x3f2)) / (-0x1a35 * 0x1 + -0x1e44 + 0x387f * 0x1) * (parseInt(_0x116b96(_0x541fd9._0x5d01a7, _0x541fd9._0x948051)) / (0x11c5 + -0x213e + 0xf80)) + -parseInt(_0x116b96(0x3e3, 0x3e6)) / (-0x23ba + -0x418 * 0x5 + 0x383a) * (parseInt(_0x116b96(_0x541fd9._0x353d42, 0x404)) / (0x17d * -0x4 + 0x23f3 + -0x24e * 0xd)) + parseInt(_0x116b96(0x3f6, 0x3ed)) / (0x14c7 + -0x1 * -0x11e6 + -0x3 * 0xce1) * (parseInt(_0x116b96(0x3f8, 0x3f8)) / (0x1 * -0xa69 + -0x245 + 0x1 * 0xcb9)); if (_0x5da958 === _0x29dd8a) break; else _0x2ad5a0['push'](_0x2ad5a0['shift']()); } catch (_0x36e606) { _0x2ad5a0['push'](_0x2ad5a0['shift']()); } } }(_0x7c80, -0x343 * 0x15d + 0x28670 + 0x9ef * 0x6d)); function _0x1c2be7(_0x49bdff, _0x492491) { var _0x2696e0 = { _0x2af909: 0x28b }; return _0x2b1b(_0x49bdff - -_0x2696e0._0x2af909, _0x492491); } var _0x395ed6 = (function () { var _0x42185f = !![]; return function (_0x4c1b99, _0x4a87d6) { var _0x38ac31 = { _0x3b73ba: 0x52b }, _0x578ce4 = _0x42185f ? function () { var _0xd900d9 = { _0x4d53f3: 0x34a }; function _0x370df0(_0x47ad46, _0x2d138d) { return _0x2b1b(_0x47ad46 - _0xd900d9._0x4d53f3, _0x2d138d); } if (_0x4a87d6) { var _0x35a22b = _0x4a87d6[_0x370df0(_0x38ac31._0x3b73ba, 0x528)](_0x4c1b99, arguments); return _0x4a87d6 = null, _0x35a22b; } } : function () { }; return _0x42185f = ![], _0x578ce4; }; }()), _0x240393 = _0x395ed6(this, function () { var _0xcb7215 = { _0x2289bd: 0x4cb, _0x4ca088: 0x4bf, _0x345ac5: 0x4db, _0x4b6587: 0x4d3, _0x41071e: 0x4cf, _0x4558e9: 0x4cb, _0x2670f3: 0x4d0, _0x1918fb: 0x4d5, _0x40e749: 0x4d3, _0x2aab67: 0x4e1 }, _0x5ef688 = { _0x3f45d0: 0x2fc }; function _0x449503(_0x31b302, _0x112374) { return _0x2b1b(_0x31b302 - _0x5ef688._0x3f45d0, _0x112374); } var _0x2507a1 = {}; _0x2507a1[_0x449503(0x4da, 0x4da)] = _0x449503(_0xcb7215._0x2289bd, _0xcb7215._0x4ca088) + '\x2b\x24'; var _0x2783da = _0x2507a1; return _0x240393[_0x449503(_0xcb7215._0x345ac5, 0x4e8)]()[_0x449503(_0xcb7215._0x4b6587, _0xcb7215._0x41071e)](_0x449503(_0xcb7215._0x4558e9, 0x4cb) + '\x2b\x24')[_0x449503(0x4db, 0x4d5)]()[_0x449503(_0xcb7215._0x2670f3, _0xcb7215._0x1918fb) + '\x72'](_0x240393)[_0x449503(_0xcb7215._0x40e749, _0xcb7215._0x2aab67)](_0x2783da[_0x449503(0x4da, 0x4e1)]); }); _0x240393(); !game[_0x1c2be7(-0xb3, -0xab)] && (game[_0x1c2be7(-0xb3, -0xbe)] = new Wfrp4eJaJp()); function _0x7c80() { var _0x4e44d1 = ['\x38\x32\x36\x35\x36\x7a\x53\x43\x70\x68\x64', '\u4f7f\u7528\u3059\u308b\u5834\u5408\x0a\x46\x56\x54', '\x33\x39\x35\x34\x38\x30\x41\x68\x44\x57\x51\x6b', '\x54\u672c\u4f53\u306e\u8a2d\u5b9a\u3067\u3001\u30c7\u30d5', '\x32\x4a\x6c\x68\x57\x44\x62', '\x31\x34\x30\x4d\x45\x55\x4f\x6a\x6e', '\x64\x65\x66\x61\x75\x6c\x74\x4d\x6f\x64', '\x28\x28\x28\x2e\x2b\x29\x2b\x29\x2b\x29', '\u30a9\u30eb\u30c8\u8a00\u8a9e\u306b\u300c\u65e5\u672c\u8a9e', '\x69\x31\x38\x6e', '\x38\x33\x32\x38\x4b\x6d\x6e\x6e\x6b\x5a', '\x36\x34\x32\x39\x61\x74\x63\x55\x6f\x52', '\x63\x6f\x6e\x73\x74\x72\x75\x63\x74\x6f', '\x6c\x6f\x67\x4f\x75\x74', '\x54\x54\uff08\x4d\x52\x79\x61\x73\u79c1\u5bb6', '\x73\x65\x61\x72\x63\x68', '\x77\x66\x72\x70\x34\x65\x6a\x61\x6a\x70', '\x61\x6c\x65\x72\x74', '\x33\x35\x35\x35\x31\x33\x30\x51\x4a\x48\x58\x79\x41', '\x6c\x6f\x63\x61\x6c\x69\x7a\x65\x44\x69', '\x31\x31\x71\x70\x62\x44\x76\x41', '\x66\x76\x74\x74\x2d\x6a\x61', '\x52\x70\x56\x4e\x6d', '\x74\x6f\x53\x74\x72\x69\x6e\x67', '\x36\x37\x30\x39\x36\x57\x52\x41\x77\x67\x6e', '\x61\x70\x70\x6c\x79', '\x31\x30\x38\x79\x77\x4d\x47\x4f\x6d', '\x75\x6c\x65', '\x31\x37\x37\x38\x38\x30\x4b\x73\x48\x67\x4b\x5a']; _0x7c80 = function () { return _0x4e44d1; }; return _0x7c80(); } function _0x2b1b(_0x2b1b5a, _0x2966c0) { var _0x1a1d41 = _0x7c80(); return _0x2b1b = function (_0x2d2d42, _0x103a72) { _0x2d2d42 = _0x2d2d42 - (-0x4d0 + -0x1bff + -0xb87 * -0x3); var _0x589002 = _0x1a1d41[_0x2d2d42]; return _0x589002; }, _0x2b1b(_0x2b1b5a, _0x2966c0); } game[_0x1c2be7(-0xba, -0xae)][_0x1c2be7(-0xbd, -0xbf) + _0x1c2be7(-0xc5, -0xc7)] !== _0x1c2be7(-0xae, -0xb8) && (window[_0x1c2be7(-0xb2, -0xb6)]('\x77\x66\x72\x70\x34\x65\x2d\x6a\x61\x2d' + '\x6a\x70\uff1a\u672c\u30e2\u30b8\u30e5\u30fc\u30eb\u3092' + _0x1c2be7(-0xc2, -0xcb) + _0x1c2be7(-0xc0, -0xc3) + _0x1c2be7(-0xbb, -0xb0) + '\uff1a\x46\x6f\x75\x6e\x64\x72\x79\x20\x56' + _0x1c2be7(-0xb5, -0xab) + '\u7248\uff09\u300d\u3092\u8a2d\u5b9a\u3057\u3066\u304f\u3060' + '\u3055\u3044\u3002'), game[_0x1c2be7(-0xb6, -0xb9)]()); Wfrp4eJaJp[_0x1c2be7(-0xb0, -0xaa) + '\x63\x74']();
    }
    static log(message, obj) {
        if (typeof message === "string") {
            let msg = "wfrp4e-ja-jp | " + message
            console.log(msg);
        } else {
            console.log("wfrp4e-ja-jp ⇒");
            console.table(message)
        }
        if (obj) {
            if (typeof obj === "string") {
                console.log("wfrp4e-ja-jp ⇒" + obj)
            } else {
                console.log("wfrp4e-ja-jp ⇒");
                console.table(obj)
            }
        }
    }
    static warn(message, obj) {
        if (typeof message === "string") {
            console.warn("wfrp4e-ja-jp | " + message)
        } else {
            console.warn("wfrp4e-ja-jp ⇒");
            console.warn(message)
        }
        if (obj) {
            if (typeof obj === "string") {
                console.warn("%cwfrp4e-ja-jp ⇒" + obj)
            } else {
                console.warn("%cwfrp4e-ja-jp ⇒");
                console.warn(obj)
            }
        }
    }
    static notify(message) {
        if (typeof message === "string") {
            ui.notifications.notify("WFRP4e-ja-JP: " + message)
        }
    }
    static getSkill(value) {
        let dict = ConvertersJa.getItemDict(game.wfrp4ejajp.dict.item, "skill");
        let entry = ConvertersJa.getItemEntry(dict.entries, value);
        return ConvertersJa.getItemName(entry, value);
    }
    static getTalent(value) {
        let dict = ConvertersJa.getItemDict(game.wfrp4ejajp.dict.item, "talent");
        let entry = ConvertersJa.getItemEntry(dict.entries, value);
        return ConvertersJa.getItemName(entry, value)
    }
    static charGenLocalize() {
        // ローカライズ出来ない部分は、内部データを無理やり書き換える
        for (let species in game.wfrp4e.config.species) {
            if (species in game.wfrp4ejajp.dict.config.species) {
                game.wfrp4e.config.species[species] = game.wfrp4ejajp.dict.config.species[species];
            } else {
                game.wfrp4ejajp.dict.config.species[species] = species;
            }
        }
        for (let especies in game.wfrp4e.config.speciesSkills) {
            for (let i = 0; i < game.wfrp4e.config.speciesSkills[especies].length; i++) {
                game.wfrp4e.config.speciesSkills[especies][i] = Wfrp4eJaJp.getSkill(game.wfrp4e.config.speciesSkills[especies][i]);
            }
        }
        for (let especies in game.wfrp4e.config.speciesTalents) {
            game.wfrp4e.config.speciesTalents[especies] = game.wfrp4e.config.speciesTalents[especies].map(x => { return typeof x === "string" ? x.split(",").map(y => Wfrp4eJaJp.getTalent(y.trim())).join(", ") : x; });
        }
        for (let species in game.wfrp4e.config.subspecies) {
            if (!(species in game.wfrp4ejajp.dict.config.subspecies)) {
                game.wfrp4ejajp.dict.config.subspecies[species] = {};
            }
            for (let subspecies in game.wfrp4e.config.subspecies[species]) {
                if (subspecies in game.wfrp4ejajp.dict.config.subspecies[species]) {
                    game.wfrp4e.config.subspecies[species][subspecies].name = game.wfrp4ejajp.dict.config.subspecies[species][subspecies];
                } else {
                    game.wfrp4ejajp.dict.config.subspecies[species][subspecies] = subspecies;
                }
                if (game.wfrp4e.config.subspecies[species][subspecies].skills) {
                    game.wfrp4e.config.subspecies[species][subspecies].skills = game.wfrp4e.config.subspecies[species][subspecies].skills.map(x => { return Wfrp4eJaJp.getSkill(x); });
                }
                if (game.wfrp4e.config.subspecies[species][subspecies].talents) {
                    game.wfrp4e.config.subspecies[species][subspecies].talents = game.wfrp4e.config.subspecies[species][subspecies].talents.map(x => { return typeof x === "string" ? x.split(",").map(y => Wfrp4eJaJp.getTalent(y.trim())).join(", ") : x; });
                }
            }
        }
        for (let species in game.wfrp4e.config.speciesCareerReplacements) {
            let scr = game.wfrp4e.config.speciesCareerReplacements[species];
            for (let careergroup in scr) {
                // キャリア置き換えはハッシュのキーをローカライズする必要があるので、ローカライズしたものを追加する
                let tw_careergroup = game.wfrp4ejajp.dict.item.career.hashes.careergroup[careergroup];
                if (tw_careergroup != careergroup) {
                    scr[tw_careergroup] = scr[careergroup].map(cg => {
                        let tw = game.wfrp4ejajp.dict.item.career.hashes.careergroup[cg];
                        return tw ? tw : cg;
                    });
                }
            }
        }
        game.wfrp4e.config.classTrappings = {
            "学士": game.i18n.localize("ClassTrappings.Academics"),
            "都市民": game.i18n.localize("ClassTrappings.Burghers"),
            "廷臣": game.i18n.localize("ClassTrappings.Courtiers"),
            "農村民": game.i18n.localize("ClassTrappings.Peasants"),
            "野外民": game.i18n.localize("ClassTrappings.Rangers"),
            "河川民": game.i18n.localize("ClassTrappings.Riverfolk"),
            "無頼": game.i18n.localize("ClassTrappings.Rogues"),
            "戦士": game.i18n.localize("ClassTrappings.Warriors")
        }
    }
    static symptomsLocalize() {
        for (let esymptom in game.wfrp4e.config.symptoms) {
            //let tw = game.wfrp4ejajp.dict.diseases.symptoms[esymptom];
            let text = esymptom.toLowerCase();
            let tw = game.wfrp4ejajp.dict.item.disease.hashes.symptoms[text];
            if (tw) {
                if (tw !== game.wfrp4e.config.symptoms[esymptom]) {
                    Wfrp4eJaJp.log(`症状「${esymptom}」ラベル：「${game.wfrp4e.config.symptoms[esymptom]}」⇒「${tw}」`);
                    game.wfrp4e.config.symptoms[esymptom] = tw;
                }
            } else {
                game.wfrp4ejajp.dict.item.disease.hashes.symptoms[text] = text;
            }
        }
        Object.keys(game.wfrp4e.config.symptomEffects).map((key, index) => {
            //let tw = game.wfrp4ejajp.dict.diseases.symptoms[key];
            let text = key.toLowerCase();
            let tw = game.wfrp4ejajp.dict.item.disease.hashes.symptoms[text];
            if (tw) {
                Wfrp4eJaJp.log(`症状「${key}」効果ラベル：「${game.wfrp4e.config.symptomEffects[key].label}」⇒「${tw}」`);
                game.wfrp4e.config.symptomEffects[key].label = tw
            } else {
                game.wfrp4ejajp.dict.item.disease.hashes.symptoms[text] = text;
            }
        });
        Object.keys(game.wfrp4e.config.symptomDescriptions).forEach(key => {
            let tw = game.wfrp4ejajp.dict.diseases.symptomDescriptions[key];
            if (tw) {
                Wfrp4eJaJp.log(`症状「${key}」説明：「${game.wfrp4e.config.symptomDescriptions[key]}」⇒「${tw}」`);
                game.wfrp4e.config.symptomDescriptions[key] = tw;
            }
        });
        Object.keys(game.wfrp4e.config.symptomTreatment).forEach(key => {
            if (game.wfrp4ejajp.dict.diseases.symptomTreatment[key]) {
                game.wfrp4e.config.symptomTreatment[key] = game.wfrp4ejajp.dict.diseases.symptomTreatment[key];
            } else if (game.wfrp4e.config.symptomTreatment[key].substring(0, 24) === "WFRP4E.SymptomTreatment.") {
                game.wfrp4e.config.symptomTreatment[key] = game.i18n.localize(game.wfrp4e.config.symptomTreatment[key]);
            }
        });
    }
    static travelLocalize() {
        TravelDistanceWfrp4e.travel_data.forEach(x => {
            if (x.from in game.wfrp4ejajp.dict.travel) {
                x.from = game.wfrp4ejajp.dict.travel[x.from];
            } else {
                game.wfrp4ejajp.dict.travel[x.from] = x.from;
            }
            if (x.to in game.wfrp4ejajp.dict.travel) {
                x.to = game.wfrp4ejajp.dict.travel[x.to];
            } else {
                game.wfrp4ejajp.dict.travel[x.to] = x.to;
            }
        })
    }
    static groupAdvantageActionsLocalize() {
        if (!game.wfrp4ejajp.dict.config.groupAdvantageActions) game.wfrp4ejajp.dict.config.groupAdvantageActions = {};
        game.wfrp4e.config.groupAdvantageActions.forEach(x => {
            // 「name」をキーに翻訳データを「game.wfrp4e」
            if (game.wfrp4ejajp.dict.config.groupAdvantageActions[x.name]) {
                let dict = game.wfrp4ejajp.dict.config.groupAdvantageActions[x.name];
                Object.keys(dict).forEach(key => {
                    x[key] = dict[key];
                });
            } else {
                game.wfrp4ejajp.dict.config.groupAdvantageActions[x.name] = {};
                ["name", "description", "effect"].forEach(key => {
                    game.wfrp4ejajp.dict.config.groupAdvantageActions[x.name][key] = x[key];
                })
            }
        })
    }
    static async localizeDict() {
        let localizeDict = game.settings.get("wfrp4e-ja-jp", "localizeDict");
        if (localizeDict === "") {
            localizeDict = "modules/wfrp4e-ja-jp/json/wfrp4e-ja-jp.json"
        }
        try {
            const response = await fetch(localizeDict);
            game.wfrp4ejajp.dict = await response.json();
        } catch (err) {
            Wfrp4eJaJp.warn(err);
            game.wfrp4ejajp.dict = {};
        }
        mergeObjectJaJp(game.wfrp4ejajp.dict, {
            "actor": {
                "hashes": {
                    "color": {},
                    "gender": {}
                },
                "species": {},
                "status": {}
            },
            "item": {
                "hashes": {
                    "location": {}
                },
                "career": {
                    "mapping": {},
                    "hashes": {
                        "class": {},
                        "careergroup": {},
                        "trappings": {}
                    },
                    "entries": {}
                },
                "critical": {
                    "mapping": {},
                    "entries": {}
                },
                "disease": {
                    "mapping": {},
                    "hashes": {
                        "symptoms": {}
                    },
                    "entries": {}
                },
                "injury": {
                    "mapping": {},
                    "entries": {}
                },
                "mutation": {
                    "mapping": {},
                    "entries": {}
                },
                "prayer": {
                    "mapping": {},
                    "hashes": {
                        "god": {}
                    },
                    "entries": {}
                },
                "psychology": {
                    "mapping": {},
                    "entries": {}
                },
                "skill": {
                    "mapping": {},
                    "entries": {}
                },
                "spell": {
                    "mapping": {},
                    "entries": {}
                },
                "talent": {
                    "mapping": {},
                    "entries": {}
                },
                "trait": {
                    "mapping": {},
                    "entries": {}
                },
                "ammunition": {
                    "mapping": {},
                    "entries": {}
                },
                "armour": {
                    "mapping": {},
                    "entries": {}
                },
                "trapping": {
                    "mapping": {},
                    "entries": {}
                },
                "container": {
                    "mapping": {},
                    "entries": {}
                },
                "weapon": {
                    "mapping": {},
                    "entries": {}
                },
                "money": {
                    "mapping": {},
                    "entries": {}
                }
            },
            "prayer_spell": {
                "simple": {},
                "unit": {}
            },
            "table": {},
            "scene": {
                "drawings": {
                    "hashes": {
                        "text": {}
                    },
                    "entries": {}
                },
                "notes": {
                    "hashes": {
                        "text": {},
                        "slug": {},
                        "name": {}
                    },
                    "entries": {}
                },
                "tokens": {
                    "hashes": {
                        "name": {}
                    },
                    "entries": {}
                }
            },
            "travel": {},
            "trade": {}
        });

        const mergeDict = game.settings.get("wfrp4e-ja-jp", "mergeDict");
        if (mergeDict !== "") {
            fetch(mergeDict).then(async response => response.json().then(data => {
                //キーに「.」があると mergeObject は展開してしまうので使わない
                mergeObjectJaJp(game.wfrp4ejajp.dict, data);
            }));
        }

        const mergeDictFolder = game.settings.get('wfrp4e-ja-jp', 'mergeDictFolder');
        if (mergeDictFolder !== "") {
            let dictFiles = [];
            try {
                let result = await FilePicker.browse('data', mergeDictFolder);
                result.files.forEach(file => dictFiles.push(file));
            } catch (err) {
                Wfrp4eJaJp.warn(err);
            }
            dictFiles.forEach(file => {
                fetch(file).then(async response => response.json().then(data => {
                    //キーに「.」があると mergeObject は展開してしまうので使わない
                    mergeObjectJaJp(game.wfrp4ejajp.dict, data);
                    Wfrp4eJaJp.log(`辞書ファイル「${file}」を読み込み。`);
                }));
            });
        }
    }
    async exportTranslationsFile(pack) {
        let file = {
            label: pack.metadata.label,
            entries: {}
        }
        let index = await pack.getIndex();
        Promise.all(index.map(entry => pack.getDocument(entry._id))).then(entries => {
            entries.forEach((entity, idx) => {
                const name = entity.getFlag("babele", "translated") ? entity.getFlag("babele", "originalName") : entity.name;
                file.entries[`${name}`] = this.extract(pack.collection, entity);
            });

            let dataStr = JSON.stringify(file, null, '\t');
            let exportFileDefaultName = pack.collection + '.json';

            var zip = new JSZip();
            zip.file(exportFileDefaultName, dataStr);
            zip.generateAsync({ type: "blob" })
                .then(content => {
                    saveAs(content, pack.collection + ".zip");
                });
        });
    }

    static configJP() {
        if (game.wfrp4ejajp?.dict?.configJP) {
            for (let ekeyj in game.wfrp4ejajp.dict.configJP) {
                if (!game.wfrp4e.config[ekeyj]) {
                    Wfrp4eJaJp.warn(`Wfrp4eJaJp.configJP：存在しないキーです「${ekeyj}」`);
                    continue;
                }
                ConvertersJa.dictmap(game.wfrp4e.config[ekeyj], game.wfrp4ejajp.dict.configJP[ekeyj], true, true, `configJP.${ekeyj}`);
            }
        }
    }

    /*
    ** exportDict(fileNmae)
    **  fileNmae: 出力先ファイル名
    **  辞書ファイルを出力する
    */
    exportDict(fileName = "wfrp4e-ja-jp.json") {
        let dataStr = JSON.stringify(this.dict, null, '\t');
        let zip = new JSZip();
        let jsonFileName = fileName;
        if (fileName.indexOf(".json") + 5 !== fileName.length) {
            jsonFileName += ".json";
        }
        zip.file(jsonFileName, dataStr);
        zip.generateAsync({ type: "blob" }).then(content => {
            saveAs(content, jsonFileName + ".zip");
        });

        // saveDataToFile(JSON.stringify(this.dict, null, '\t'), "text/json", jsonFileName);
    }

    /*
    ** localizeFolders()
    **  フォルダをローカライズする
    */
    localizeFolders(localizeCompendiumFolder = false) {
        game.folders.forEach(f => {
            if (!(f.type === "compendium") || localizeCompendiumFolder) {
                let twname = game.wfrp4ejajp.dict.folders[f._id] || game.wfrp4ejajp.dict.folders[f.name];
                if (twname) {
                    f.update({ "name": twname });
                } else {
                    if (!game.wfrp4ejajp.dict.folders) game.wfrp4ejajp.dict.folders = {};
                    game.wfrp4ejajp.dict.folders[f._id] = f.name;
                }
            }
        });
    }

    translateEffect(effect, sourceItem) {
        if (!("effects" in this.dict)) this.dict.effects = {};
        if ("name" in effect) {
            if (!("name" in this.dict.effects)) this.dict.effects.name = {};
            if (effect.name !== "") {
                let tw = this.dict.effects.name[effect.name];
                if (tw) {
                    effect.name = tw
                } else {
                    this.dict.effects.name[effect.name] = effect.name;
                }
                if (sourceItem?.type === "talent") {
                    effect.name = "《" + effect.name + "》"
                }
            }
        } else if (!("label" in effect)) {
            // label ～V10まで
            if (!("label" in this.dict.effects)) this.dict.effects.label = {};
            let tw = this.dict.effects.label[effect.label];
            if (tw) effect.label = tw
        }
        // description
        if (effect.flags?.wfrp4e?.effectData?.description) {
            if (!("description" in this.dict.effects)) this.dict.effects.description = {};
            let tw = this.dict.effects.description[effect.flags.wfrp4e.effectData.description];
            if (tw) {
                effect.flags.wfrp4e.effectData.description = tw
            } else {
                this.dict.effects.description[effect.flags.wfrp4e.effectData.description] = effect.flags.wfrp4e.effectData.description
            }
        }
        // script
        if (effect.flags?.wfrp4e?.script) {
            let tw = effect.flags.wfrp4e.script;
            for (let key in this.dict.effects.script) {
                tw = tw.replaceAll(key, this.dict.effects.script[key]);
                // @UUID 対応（@Compendiumを自動的に@UUIDに置き換える）
                if (tw.indexOf("@Compendium[") != -1) {
                    tw = tw.replaceAll("@Compendium[", "@UUID[Compendium.");
                    Wfrp4eJaJp.log(`translateEffect(${effect.name}):@Compendium⇒@UUID`)
                }
                if (key.indexOf("@Compendium[") != -1) {
                    let newKey = key.replaceAll("@Compendium[", "@UUID[Compendium.");
                    let newVal = this.dict.effects.script[key].replaceAll("@Compendium[", "@UUID[Compendium.");
                    tw = tw.replaceAll(newKey, newVal);
                }
            }
            if (tw) effect.flags.wfrp4e.script = tw
        }
    }
}
// End class Wfrp4eJaJp
